package wallets;

import commons.Coins;
import wallets.exceptions.WalletRestPreparingException;

import java.util.Map;

public interface IWallet {
    Map<Coins, Integer> getRest(int amount, int productPrice) throws WalletRestPreparingException;
}
